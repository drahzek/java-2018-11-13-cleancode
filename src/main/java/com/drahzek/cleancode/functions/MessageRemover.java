package com.drahzek.cleancode.functions;

import com.drahzek.cleancode.model.Message;

public interface MessageRemover {

    void removeMessage(Message message);
}
