package com.drahzek.cleancode.functions;

import com.drahzek.cleancode.model.Message;

public interface MessageAdder {

    void addMessage(Message message);
}
