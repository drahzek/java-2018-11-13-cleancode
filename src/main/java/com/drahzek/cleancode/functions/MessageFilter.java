package com.drahzek.cleancode.functions;

import com.drahzek.cleancode.model.Message;

public interface MessageFilter {

    void filterMessage(Message message);
}
