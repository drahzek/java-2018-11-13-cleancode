package com.drahzek.cleancode.functions;

import com.drahzek.cleancode.model.Message;

public interface MessageShow {

    void showMessage(Message message);
}
