package com.drahzek.cleancode.model;

public class MessageRepository {

    private MessageStorage messageStorage;

    public MessageRepository(MessageStorage messageStorage) {
        this.messageStorage = messageStorage;
    }

    public void addMessage(Message message) {
        messageStorage.addMessage(message);
    }

    public boolean removeMessage(Message message) {
        return messageStorage.removeMessage(message);
    }
}
