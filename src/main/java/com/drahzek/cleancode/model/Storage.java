package com.drahzek.cleancode.model;

public interface Storage {

    void addMessage(Message message);
    boolean removeMessage(Message message);
}
