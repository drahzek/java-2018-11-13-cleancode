package com.drahzek.cleancode.model;

import java.util.Scanner;

public class Application {


    public void start(){
        System.out.println("Message management app");

        Scanner input = new Scanner(System.in);

        boolean running = true;

        int command;

        while(running) {//Application main loop.

            System.out.println("0 - exit\n" +
                    "1 - add message\n" +
                    "2 - show message\n" +
                    "3 - delete message\n" +
                    "4 - filter messages");

            System.out.print("Command: ");
            command = input.nextInt();

            switch (command) {
                case 1: {
                    addMessage(input);
                    break;
                }
                case 2: {
                    showMessage();
                    break;
                }
                case 3: {//Save notes to file.
                    input.nextLine();//Consume \n after command.
                    saveNotes(input);
                    break;
                }
                case 4: {//Read notes from file.
                    input.nextLine();//Consume \n after command.
                    readNotes(input);
                    break;
                }
                case 0: {//Exit application.
                    running = false;
                    break;
                }
            }
        }
    }
}
