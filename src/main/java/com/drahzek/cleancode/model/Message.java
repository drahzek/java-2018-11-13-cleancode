package com.drahzek.cleancode.model;

import java.util.Date;

public class Message {

    private String title;
    private String author;
    private String content;
    private Date creationDate;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public Date getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(Date creationDate) {
        this.creationDate = creationDate;
    }

    public Message(String title, String author, String content, Date creationDate) {
        this.title = title;
        this.author = author;
        this.content = content;
        this.creationDate = creationDate;
    }
}
