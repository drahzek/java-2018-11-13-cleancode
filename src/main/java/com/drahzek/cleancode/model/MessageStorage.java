package com.drahzek.cleancode.model;

import java.util.ArrayList;
import java.util.List;

public class MessageStorage implements Storage{

    private List<Message> dataStorage = new ArrayList<>();

    @Override
    public void addMessage (Message message) {
        if (message == null) {
            throw new RuntimeException("Error");
        }
        dataStorage.add(message);
    }

    @Override
    public boolean removeMessage (Message message) {
        return dataStorage.remove(message);
    }
}
