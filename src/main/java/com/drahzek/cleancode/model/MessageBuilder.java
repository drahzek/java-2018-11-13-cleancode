package com.drahzek.cleancode.model;

import java.util.Date;

public class MessageBuilder {
    private String title;
    private String author;
    private String content;
    private Date creationDate;

    public MessageBuilder setTitle(String title) {
        this.title = title;
        return this;
    }

    public MessageBuilder setAuthor(String author) {
        this.author = author;
        return this;
    }

    public MessageBuilder setContent(String content) {
        this.content = content;
        return this;
    }

    public MessageBuilder setCreationDate(Date creationDate) {
        this.creationDate = creationDate;
        return this;
    }

    public Message createMessage() {
        return new Message(title, author, content, creationDate);
    }
}